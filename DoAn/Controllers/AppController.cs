﻿using DoAn.Dtos.App;
using DoAn.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DoAn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppController : ApiControllerBase
    {
        private readonly IAppService _appService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public AppController(IAppService appService, IWebHostEnvironment webHostEnvironment, ILogger<AppController> logger) : base(logger)
        {
            _appService = appService;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("create-app")]
        public IActionResult CreateApp(CreateAppDto input)
        {
            try
            {
                int id = _appService.CreateApp(input);
                return Ok(id);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPost("[action]")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue, MemoryBufferThreshold = int.MaxValue)]
        [RequestSizeLimit(int.MaxValue)]
        public IActionResult UploadIcon([FromQuery] int id)
        {
            try
            {
                var file = Request.Form.Files[0];
                _appService.UploadIcon(file, _webHostEnvironment.ContentRootPath, id);
                return Ok(_appService);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        [HttpPost("[action]")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue, MemoryBufferThreshold = int.MaxValue)]
        [RequestSizeLimit(int.MaxValue)]
        public IActionResult UploadWallpaper([FromQuery] int id)
        {
            try
            {
                var file = Request.Form.Files[0];
                _appService.UploadWallpaper(file, _webHostEnvironment.ContentRootPath, id);
                return Ok(_appService);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        [HttpGet("get-all-app")]
        [Authorize(Roles = "1, 2")]
        public IActionResult GetAllApp()
        {
            try
            {
                var apps = _appService.GetAllApp();
                return Ok(apps);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-app-by-id")]
        [Authorize(Roles = "1, 2")]
        public IActionResult GetAppById(int id)
        {
            try
            {
                var app = _appService.GetAppById(id);
                return Ok(app);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-app-by-publisher")]
        public IActionResult GetAppByPublisher(string publisher)
        {
            try
            {
                var app = _appService.GetAppByPublisher(publisher);
                return Ok(app);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-app-by-tag")]
        [Authorize(Roles = "1, 2")]
        public IActionResult GetAppByTag(string tag)
        {
            try
            {
                var apps = _appService.GetAppByTag(tag);
                return Ok(apps);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-category")]
        [Authorize(Roles = "1, 2")]
        public IActionResult GetCategory()
        {
            try
            {
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult DownloadIcon([FromQuery] int id)
        {
            //string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");
            //string filePath = Path.Combine(directoryPath, fileName);
            string filePath = _appService.DownloadIcon(id);

            if (filePath != "" && System.IO.File.Exists(filePath))
            {
                //return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath));
                return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath), enableRangeProcessing: true);
            }
            return NotFound();
        }

        [HttpGet("[action]")]
        public IActionResult DownloadWallpaper([FromQuery] int id)
        {
            //string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");
            //string filePath = Path.Combine(directoryPath, fileName);
            string filePath = _appService.DownloadWallpaper(id);

            if (filePath != "" && System.IO.File.Exists(filePath))
            {
                //return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath));
                return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath), enableRangeProcessing: true);
            }
            return NotFound();
        }

        [HttpGet("[action]")]
        public IActionResult DownloadFileSetup([FromQuery] int id)
        {
            //string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");
            //string filePath = Path.Combine(directoryPath, fileName);
            string filePath = _appService.DownloadFileSetup(id);

            if (filePath != "" && System.IO.File.Exists(filePath))
            {
                //return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath));
                return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath), enableRangeProcessing: true);
            }
            return NotFound();
        }

        [HttpPost("[action]")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue, MemoryBufferThreshold = int.MaxValue)]
        [RequestSizeLimit(int.MaxValue)]
        public IActionResult UploadFileSetup([FromQuery] int id)
        {
            try
            {
                var file = Request.Form.Files[0];
                _appService.UploadFileSetup(file, _webHostEnvironment.ContentRootPath, id);
                return Ok(_appService);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
    }
}
