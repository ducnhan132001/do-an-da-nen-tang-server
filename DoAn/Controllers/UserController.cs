﻿using DoAn.Dtos.Users;
using DoAn.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DoAn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ApiControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService, ILogger<UserController> logger) : base(logger)
        {
            _userService = userService;
        }

        [HttpPost("signin")]
        public IActionResult Create(CreateUserDto input)
        {
            try
            {
                _userService.Create(input);
                return Ok();
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPost("login")]
        public IActionResult Login(LoginDto input)
        {
            try
            {
                string token = _userService.Login(input);
                return Ok(new { token });
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("test")]
        [Authorize(Roles = "1")]
        public IActionResult TestToken()
        {
            try
            {
                return Ok("Da xac thuc dang nhap");
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpGet("get-user-type")]
        [Authorize(Roles = "1, 2")]
        public IActionResult GetUserType(string userName)
        {
            try
            {
                string userType = _userService.GetTypeUser(userName);
                return Ok(userType);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPost("become-publisher")]
        [Authorize(Roles = "2")]
        public IActionResult BecomePublisher(string userName)
        {
            try
            {
                _userService.BecomePublisher(userName);
                return Ok(_userService);
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }
    }
}
