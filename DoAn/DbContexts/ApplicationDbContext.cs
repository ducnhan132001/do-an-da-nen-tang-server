﻿using DoAn.Constants;
using DoAn.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace DoAn.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<App> Apps { get; set; }

        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Username)
                    .IsUnicode(false)
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Password)
                    .IsUnicode()
                    .HasMaxLength(100)
                    .IsRequired();
                entity.Property(e => e.UserType)
                    .HasDefaultValue(UserTypes.Customer)
                    .IsRequired();
            });

            modelBuilder.Entity<App>(entity =>
            {
                entity.ToTable("App");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Name)
                    .IsUnicode(false)
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.PathToIcon)
                    .IsUnicode()
                    .HasMaxLength(200)
                    .IsRequired();
                entity.Property(e => e.PathToWallpaper)
                    .IsUnicode()
                    .HasMaxLength(200)
                    .IsRequired();
                entity.Property(e => e.Rating)
                    .IsUnicode()
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Description)
                    .IsUnicode()
                    .HasMaxLength(1000)
                    .IsRequired();
                entity.Property(e => e.DownloadCount)
                    .IsRequired();
                entity.Property(e => e.Type)
                    .IsUnicode()
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Publisher)
                    .IsUnicode()
                    .HasMaxLength(50)
                    .IsRequired();
                entity.Property(e => e.Tag)
                    .IsUnicode()
                    .HasMaxLength(100)
                    .IsRequired();
                entity.Property(e => e.PathToFileApp)
                    .IsUnicode()
                    .HasMaxLength(200)
                    .IsRequired();
            });
        }
    }
}
