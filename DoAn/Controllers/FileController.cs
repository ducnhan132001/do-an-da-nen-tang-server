﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;

namespace DoAn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ApiControllerBase
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public FileController(ILogger<FileController> logger, IWebHostEnvironment webHostEnvironment) : base(logger)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("[action]")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue, MemoryBufferThreshold = int.MaxValue)]
        [RequestSizeLimit(int.MaxValue)]
        public IActionResult UploadFile(IFormFile file)
        {
            try
            {
                if (file == null)
                {
                    return Ok("Khong co file");
                }

                string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");

                string filePath = Path.Combine(directoryPath, file.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                return Ok("Upload Successful");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue, MemoryBufferThreshold = int.MaxValue)]
        [RequestSizeLimit(int.MaxValue)]
        public IActionResult Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");
                var folderName = Path.Combine(directoryPath, "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName?.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName != null ? fileName : "");
                    var dbPath = Path.Combine(folderName, fileName != null ? fileName : "");
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    return Ok(new { dbPath });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        [HttpGet("[action]")]
        public IActionResult DownloadFile([FromQuery]string fileName)
        {
            //try
            //{
            //    //string path = "\path\to\the\file.txt";
            //    string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");
            //    string filePath = Path.Combine(directoryPath, fileName);

            //    if (System.IO.File.Exists(filePath))
            //    {
            //        //return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath));
            //        return File(_webHostEnvironment
            //                    .WebRootFileProvider
            //                    .GetFileInfo(filePath)
            //                    .CreateReadStream(),
            //                    "application/octet-stream",
            //                    enableRangeProcessing: true);
            //    }
            //    return NotFound();
            //}
            //catch (Exception ex)
            //{
            //    //_logger.LogError(ex, $"Error while downloading the file");
            //    return StatusCode(StatusCodes.Status500InternalServerError);
            //    throw;
            //}
            string directoryPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources");
            string filePath = Path.Combine(directoryPath, fileName);

            if (System.IO.File.Exists(filePath))
            {
                //return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath));
                return File(System.IO.File.OpenRead(filePath), "application/octet-stream", Path.GetFileName(filePath), enableRangeProcessing: true);
            }
            return NotFound();
        }

        [HttpGet("Test")]
        public IActionResult Test()
        {
            return Ok("Oke");
        }
    }
}
