﻿using Azure;
using DoAn.DbContexts;
using DoAn.Dtos.App;
using DoAn.Entities;
using DoAn.Exceptions;
using DoAn.Services.Interfaces;
using DoAn.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;

namespace DoAn.Services.Implements
{
    public class AppService : IAppService
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IConfiguration _configuration;

        public AppService(
            ILogger<AppService> logger,
            ApplicationDbContext dbContext,
            IConfiguration configuration)
        {
            _logger = logger;
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public int CreateApp(CreateAppDto input)
        {
            var app = new App
            {
                Name = input.Name,
                PathToIcon = "",
                PathToWallpaper = "",
                Rating = "0",
                Description = input.Description,
                DownloadCount = 0,
                Type = input.Type,
                Publisher = input.Publisher,
                Tag = input.Tag,
                PathToFileApp = "",
            };
            _dbContext.Apps.Add(app);
            _dbContext.SaveChanges();
            return app.Id;
        }

        public void UploadIcon(IFormFile file, string path, int id)
        {
            var app = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (app == null)
            {
                throw new UserFriendlyException("Không tìm thấy ứng dụng");
            }
            if (file.Length <= 0)
            {
                throw new UserFriendlyException("File không hợp lệ");
            }
            string directoryPath = Path.Combine(path, "Resources");
            var folderName = Path.Combine(directoryPath, "Images/Icons");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName?.Trim('"');
            var fullPath = Path.Combine(pathToSave, fileName != null ? fileName : "");
            var dbPath = Path.Combine(folderName, fileName != null ? fileName : "");
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            app.PathToIcon = dbPath;
            _dbContext.SaveChanges();
        }

        public void UploadWallpaper(IFormFile file, string path, int id)
        {
            var app = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (app == null)
            {
                throw new UserFriendlyException("Không tìm thấy ứng dụng");
            }
            if (file.Length <= 0)
            {
                throw new UserFriendlyException("File không hợp lệ");
            }
            string directoryPath = Path.Combine(path, "Resources");
            var folderName = Path.Combine(directoryPath, "Images/Wallpapers");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName?.Trim('"');
            var fullPath = Path.Combine(pathToSave, fileName != null ? fileName : "");
            var dbPath = Path.Combine(folderName, fileName != null ? fileName : "");
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            app.PathToWallpaper = dbPath;
            _dbContext.SaveChanges();
        }

        public List<App> GetAllApp()
        {
            var apps = _dbContext.Apps.AsQueryable();
            if (apps.Count() <= 0)
                throw new UserFriendlyException("Không có app trong database");
            return apps.ToList();
        }

        public List<App> GetAppByTag(string tag)
        {
            var appsByTag = _dbContext.Apps.AsQueryable();
            appsByTag = appsByTag.Where((app) => app.Tag == tag);
            if (appsByTag.Count() <= 0)
                throw new UserFriendlyException("Không có app theo tag");
            return appsByTag.ToList();
        }

        public List<App> GetAppByPublisher(string publisher)
        {
            var appsByTag = _dbContext.Apps.AsQueryable();
            appsByTag = appsByTag.Where((app) => app.Publisher == publisher);
            if (appsByTag.Count() <= 0)
                throw new UserFriendlyException("Không có app theo publisher");
            return appsByTag.ToList();
        }

        public App GetAppById(int id)
        {
            var appById = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (appById != null)
            {
                return appById;
            }
            else
            {
                throw new UserFriendlyException("Id app không hợp lệ");
            }
        }

        public string DownloadIcon(int id)
        {
            var appById = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (appById != null)
            {
                return appById.PathToIcon;
            }
            else
            {
                return "";
            }
        }

        public string DownloadWallpaper(int id)
        {
            var appById = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (appById != null)
            {
                return appById.PathToWallpaper;
            }
            else
            {
                return "";
            }
        }

        public void UploadFileSetup(IFormFile file, string path, int id)
        {
            var app = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (app == null)
            {
                throw new UserFriendlyException("Không tìm thấy ứng dụng");
            }
            if (file.Length <= 0)
            {
                throw new UserFriendlyException("File không hợp lệ");
            }
            string directoryPath = Path.Combine(path, "Resources");
            var folderName = Path.Combine(directoryPath, "FileSetup");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName?.Trim('"');
            var fullPath = Path.Combine(pathToSave, fileName != null ? fileName : "");
            var dbPath = Path.Combine(folderName, fileName != null ? fileName : "");
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            app.PathToFileApp = dbPath;
            _dbContext.SaveChanges();
        }

        public string DownloadFileSetup(int id)
        {
            var appById = _dbContext.Apps.FirstOrDefault((app) => app.Id == id);
            if (appById != null)
            {
                return appById.PathToFileApp;
            }
            else
            {
                return "";
            }
        }
    }
}
