﻿namespace DoAn.Entities
{
    public class App
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PathToIcon { get; set; }
        public string PathToWallpaper { get; set; }
        public string Rating { get; set; }
        public string Description { get; set; }
        public int DownloadCount { get; set; }
        public string Type { get; set; }//Thể loại của app rpg action or gì đó
        public string Publisher { get; set; }
        public string Tag { get; set; }
        public string PathToFileApp { get; set; }
    }
}
