﻿using DoAn.Dtos.App;
using DoAn.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DoAn.Services.Interfaces
{
    public interface IAppService
    {
        int CreateApp(CreateAppDto input);
        void UploadIcon(IFormFile file, string path, int id);
        string DownloadIcon(int id);
        void UploadWallpaper(IFormFile file, string path, int id);
        string DownloadWallpaper(int id);
        void UploadFileSetup(IFormFile file, string path, int id);
        string DownloadFileSetup(int id);
        List<App> GetAllApp();
        List<App> GetAppByTag(string tag);
        List<App> GetAppByPublisher(string publisher);
        App GetAppById(int id);
    }
}
