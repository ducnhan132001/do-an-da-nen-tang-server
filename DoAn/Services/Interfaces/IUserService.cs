﻿using DoAn.Dtos.Users;
using Microsoft.AspNetCore.Mvc;

namespace DoAn.Services.Interfaces
{
    public interface IUserService
    {
        void Create(CreateUserDto input);
        string Login(LoginDto input);
        string GetTypeUser(string userName);
        void BecomePublisher(string userName);
    }
}
