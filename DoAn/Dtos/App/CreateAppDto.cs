﻿namespace DoAn.Dtos.App
{
    public class CreateAppDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Publisher { get; set; }
        public string Tag { get; set; }
    }
}
